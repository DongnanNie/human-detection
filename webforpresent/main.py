import sys
import os
import numpy as np
import tensorflow as tf

from keras.applications.imagenet_utils import preprocess_input, decode_predictions
from keras.preprocessing import image
from keras.applications.mobilenet import MobileNet

from flask import Flask, redirect, url_for, request, render_template, Response
from werkzeug.utils import secure_filename

import cv2

import os
import sys
import random
import math
import numpy as np
import skimage.io
import matplotlib
import matplotlib.pyplot as plt
from flask import Flask, render_template, Response
import cv2
import numpy as np
import skimage.io
import matplotlib.image

# Root directory of the project
ROOT_DIR = os.path.abspath("../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn import utils
import mrcnn.model as modellib
from mrcnn import visualize1
from mrcnn import visualize2
# Import COCO config
sys.path.append(os.path.join(ROOT_DIR, "samples/coco/"))  # To find local version
import coco



# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

# Local path to trained weights file
COCO_MODEL_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")
# Download COCO trained weights from Releases if needed
#if not os.path.exists(COCO_MODEL_PATH):
   # utils.download_trained_weights(COCO_MODEL_PATH)

# Directory of images to run detection on
IMAGE_DIR = os.path.join(ROOT_DIR, "images")

class InferenceConfig(coco.CocoConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

config = InferenceConfig()
# Create model object in inference mode.
model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)

# Load weights trained on MS-COCO
model.load_weights(COCO_MODEL_PATH, by_name=True)


# COCO Class names
# Index of the class in the list is its ID. For example, to get ID of
# the teddy bear class, use: class_names.index('teddy bear')
class_names = ['N', 'person']

# Define a flask app and configure an upload directory
app = Flask('ImageBot')
app.config['UPLOAD_FOLDER'] = './static/uploads/'
app.config['test'] = './'
# Load your model
#model = MobileNet(weights='imagenet')
# Fixing an error with servings
global graph
graph = tf.get_default_graph()

# Function to pipe model predictions
def model_predict(img_path, model):
	# Load the image
	#img = image.load_img(img_path, target_size=(224,224))

	# Preprocess the image
	#x = image.img_to_array(img)
	#x = np.expand_dims(x, axis=0)
	#x = preprocess_input(x, mode='caffe')

	with graph.as_default():
		#----------------------
		image = skimage.io.imread(os.path.join(img_path))

		# Run detection
		results = model.detect([image], verbose=1)

		print('-----------------------------------------------------------')

		# Visualize results
		r = results[0]
		print('----------visualize--------start----------------------')
		image = visualize1.display_instances(image, r['rois'], r['masks'], r['class_ids'],
										 class_names, r['scores'])

		i = 0
		cp = 0

		while i < len(results[0]['class_ids']):
			if results[0]['class_ids'][i] == 1:
				# print(results[0]['class_ids'][i],results[0]['scores'][i])
				if results[0]['scores'][i] > 0.8:
					cp = cp + 1
			i = i + 1

		prediction = cp


		#----------------------

		#prediction = model.predict(x)
	return prediction

# Generate the HTML template that we have constructed
@app.route('/')
def hello():
    return render_template('index.html')

# Generate the prediction from the uploads folder
@app.route('/', methods=['GET', 'POST'])
def upload_file():
	if request.method == 'POST':
		# Get the file from post request
		f = request.files['file']

		# Save the file to ./uploads
		file_path = os.path.join(
			app.config['UPLOAD_FOLDER'], secure_filename(f.filename))
		f.save(file_path)
		print(file_path)
		file_path_result = os.path.join(app.config['test'], 'test.jpeg')
		print(file_path_result)

		abs_file_path = os.path.abspath(file_path)

        # Make prediction
		preds = model_predict(file_path, model)

		# Decode the predictions to a string format
		#pred_class = decode_predictions(preds, top=1)
		#result = str(pred_class[0][0][1])

		return render_template('doc.html', result = preds, url=file_path_result)
		# return render_template('doc.html', url = file_path, result = result)
	return render_template('index.html')

class VideoCamera(object):
    def __init__(self):
        # éè¿opencvè·åå®æ¶è§é¢æµ
        self.video = cv2.VideoCapture("http://192.168.137.2:8081/videostream.cgi?.mjpg")

    def __del__(self):
        self.video.release()

    def get_frame(self):
        success, image = self.video.read()
        # å ä¸ºopencvè¯»åçå¾çå¹¶éjpegæ ¼å¼ï¼å æ­¤è¦ç¨motion JPEGæ¨¡å¼éè¦åå°å¾çè½¬ç æjpgæ ¼å¼å¾ç
        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()

def gen(camera):
    while True:
        frame = camera.get_frame()
        # ä½¿ç¨generatorå½æ°è¾åºè§é¢æµï¼ æ¯æ¬¡è¯·æ±è¾åºçcontentç±»åæ¯image/jpeg
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


@app.route('/video_feed')  # è¿ä¸ªå°åè¿åè§é¢æµååº
def video_feed():
    return Response(gen(VideoCamera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
	app.run(host='0.0.0.0', debug = True)