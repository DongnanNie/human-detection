from keras.applications.imagenet_utils import preprocess_input, decode_predictions
from keras.preprocessing import image
from keras.applications.mobilenet import MobileNet
import numpy as np

import os
import sys
import random
import math
import numpy as np
import skimage.io
import matplotlib
import matplotlib.pyplot as plt
from flask import Flask, render_template, Response
import cv2
import numpy as np
import skimage.io
import matplotlib.image

# Root directory of the project
ROOT_DIR = os.path.abspath("../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn import utils
import mrcnn.model as modellib
from mrcnn import visualize1
from mrcnn import visualize2
# Import COCO config
#sys.path.append(os.path.join(ROOT_DIR, "samples/coco/"))  # To find local version
import coco

%matplotlib inline

# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

# Local path to trained weights file
COCO_MODEL_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")
# Download COCO trained weights from Releases if needed
#if not os.path.exists(COCO_MODEL_PATH):
   # utils.download_trained_weights(COCO_MODEL_PATH)

# Directory of images to run detection on
IMAGE_DIR = os.path.join(ROOT_DIR, "images")

class InferenceConfig(coco.CocoConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

config = InferenceConfig()
# Create model object in inference mode.
modeldetect = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)

# Load weights trained on MS-COCO
modeldetect.load_weights(COCO_MODEL_PATH, by_name=True)


# COCO Class names
# Index of the class in the list is its ID. For example, to get ID of
# the teddy bear class, use: class_names.index('teddy bear')
class_names = ['N', 'person']

#-----------------
model = MobileNet(weights='imagenet')

def model_predict(img_path, model):
	# Load the image
	img = image.load_img(img_path, target_size=(224,224))

	# Preprocess the image
	x = image.img_to_array(img)
	x = np.expand_dims(x, axis=0)
	x = preprocess_input(x, mode='caffe')

	prediction = model.predict(x)

	results = model.detect([frame], verbose=1)
	r = results[0]
	masked_image = visualize1.display_instances(frame, r['rois'], r['masks'], r['class_ids'],
												class_names, r['scores'])

	return prediction

def upload_file(file_path):
	preds = model_predict(file_path, model)
	# Decode the predictions to a string format
	pred_class = decode_predictions(preds, top=1)  
	result = str(pred_class[0][0][1])  
	print(result)

if __name__ == '__main__':
	file_path = input('Where is your image: ')
	upload_file(file_path)